package ru.t1.didyk.taskmanager.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
