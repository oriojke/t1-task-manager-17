package ru.t1.didyk.taskmanager.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect.");
    }

}
